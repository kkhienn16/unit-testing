const { factorial } = require('../src/util.js');

// Gets the expect and assert functions from chai to be used
const { expect, assert } = require('chai');

// Test Suites are made up of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {

	// test cases
	// 'it ()' accepts two parameters
		// first param string explaining what the test should do
		//second param callback function which contains the actual test

	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		// "expect" - returning expected and actual value
		expect(product).to.equal(120);

	});


	it('test_fun_factorial_1!_is_1', ()=> {
		const product = factorial(1);
		//  "assert" - checking if function is return correct results
		assert.equal(product, 1);
	});
})