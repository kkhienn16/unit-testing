const { factorial, div_check } = require('../src/util.js');

// Gets the expect and assert functions from chai to be used
const { expect, assert } = require('chai');

// Test Suites are made up of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {

	// test cases
	// 'it ()' accepts two parameters
		// first param string explaining what the test should do
		//second param callback function which contains the actual test

	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		// "expect" - returning expected and actual value
		expect(product).to.equal(120);

	});


	it('test_fun_factorial_1!_is_1', ()=> {
		const product = factorial(1);
		//  "assert" - checking if function is return correct results
		assert.equal(product, 1);
	});

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		// "expect" - returning expected and actual value
		assert.equal(product, 1);

	});

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		// "expect" - returning expected and actual value
		expect(product).to.equal(24);

	});

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		// "expect" - returning expected and actual value
		expect(product).to.equal(3628800);

	});
	// Test for negative numbers
	it('test_fun_factorial_neg_1_is_undefined',() => {
		// RED - Write a test thhat fails
		const product = factorial(-1);
		expect(product).to.equal(undefined);
	})

	it('test_fun_factorial_invalid_number_is_undefined',() => {

		const product = factorial('num');
		expect(product).to.equal(undefined)
	})

})


/*
S2 Activity Instructions:

1. In your test.js, create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.
2. In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.
a. If the number received is divisible by 5, return true.
b. If the number received is divisible by 7, return true.
c. Return false if otherwise
3. Create 4 test cases in a new test suite in test.js that would check if the functionality of div_check is correct.
4. Initialize your local git repository, add the remote link and push to git with the commit message of “Add activity code S2".
5. Add the link in Boodle.

*/


// ACTIVITY

describe('test_divisibility_by_5_or_7', () => {

	it('test_100_is_divisible_by_5', () => {
		const divisible = div_check(100);
		expect(divisible).to.equal(true);
	});

	it('test_49_is_divisible_by_7', () => {
		const divisible = div_check(49);
		expect(divisible).to.equal(true);
	})

	it('test_30_is_divisible_by_5', () => {
		const divisible = div_check(30);
		expect(divisible).to.equal(true);
	})

	it('test_56_is_divisible_by_7', () => {
		const divisible = div_check(56);
		expect(divisible).to.equal(true);
	})
})

