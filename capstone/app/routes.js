const { exchangeRates, currency } = require('../src/util.js');

module.exports = (app) => {
  app.get('/', (req, res) => {
    return res.send({'data': {} });
  });

  app.get('/rates', (req, res) => {
    return res.send({
      rates: exchangeRates
    });
  })

  // Start

  app.post('/currency',(req,res) => {

    const euroData = req.body.euro;
    const nameType = typeof euroData.name;
    const aliasType = typeof euroData.alias;


    // 1. Check if post /currency is running
    if (euroData && 
        euroData.hasOwnProperty('name') && 
        euroData.hasOwnProperty('alias')&& 
        typeof euroData.alias === 'string' &&
        euroData.alias !== "" &&
        euroData.hasOwnProperty('ex') &&
        nameType === 'string' && 
        euroData.name !== "" && 
        typeof euroData.ex !== 'string' && 
        Object.keys(euroData.ex).length !== 0 )  {
    
      
    
      return res.status(200).json({ 'message': 'Currency added successfully', 'nameType': nameType });
    }

    //2. Check if post /currency returns status 400 if name is missing
    if (!euroData.name && euroData.name !== "") {
      // Return a 400 Bad Request response
      return res.status(400).send({
        'error': 'Bad Request - missing required parameter NAME'
      });
    }

    //3. Check if post /currency returns status 400 if name is not a string
    if (nameType !== 'string') {
      // Return a 400 Bad Request response

      return res.status(400).send({
        'error': 'Bad Request - NAME is not a string'
      });
    }

    // 4. Check if post /currency returns status 400 if name is empty
    if (euroData.name === "") {
      // Return a 400 Bad Request response
      return res.status(400).send({
        'error': 'Bad Request - NAME cannot be empty'
      });
    }

    // 5. Check if post /currency returns status 400 if ex is missing
    if (!euroData || !euroData.ex) {
      // Return a 400 Bad Request response
      return res.status(400).send({
        'error': 'Bad Request - missing required parameter EX'
      });
    }

    // 6. Check if post /currency returns status 400 if ex is not an object
    if (typeof euroData.ex !== 'object') {
      // Return a 400 Bad Request response
      return res.status(400).send({
        'error': 'Bad Request - EX is not an object'
      });
    }

    // 7. Check if post /currency returns status 400 if ex is empty
    if (Object.keys(euroData.ex).length === 0) {
      // Return a 400 Bad Request response
      return res.status(400).send({
        'error': 'Bad Request - EX cannot be empty'
      });
    }

    //8. Check if post /currency returns status 400 if alias is missing
    if (!euroData || !euroData.hasOwnProperty('alias')) {
      // Return a 400 Bad Request response
      return res.status(400).send({
        'error': 'Bad Request - missing required parameter ALIAS'
      });
    }

    //9. Check if post /currency returns status 400 if alias is not an string
    if (aliasType !== 'string') {
        // Return a 400 Bad Request response
        return res.status(400).send({
          'error': 'Bad Request - ALIAS is not a string'
        });
      }

   // 10. Check if post /currency returns status 400 if alias is empty
    if (euroData.name === "") {
      // Return a 400 Bad Request response
      return res.status(400).send({
        'error': 'Bad Request - ALIAS cannot be empty'
      });
    }

    // Convert the properties of 'currency' object into an array of objects
    const currencyArray = Object.values(currency);

    // 11. Check if post /currency returns status 400 if all fields are complete 
    let foundAlias = currencyArray.find((currencyObj) => {
      return currencyObj.alias === req.body.alias &&
             currencyObj.name === req.body.name ;
    });

    if (euroData.alias === foundAlias) {
      return res.status(400).send({
        'error': "Duplicated Alias"
      });
    }

    // 12. Check if post /currency returns status 200 if all fields are complete and no duplicates
   if (
      euroData.euro &&
      euroData.name &&
      euroData.alias &&
      euroData.ex &&
      typeof euroData.name === 'string' &&
      euroData.name.trim() !== '' &&
      typeof euroData.alias === 'string' &&
      euroData.alias !== foundAlias &&
      euroData.alias.trim() !== '' &&
      typeof euroData.ex === 'object' &&
      Object.keys(req.body.euro.ex).length > 0
    ) {
      return res.status(200).json({ 'message': 'Currency added successfully' });
    }
    

  })

}
