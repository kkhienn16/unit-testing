const chai = require('chai');
const expect = chai.expect;
const { exchangeRates } = require('../src/util.js');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})

	// Start
	// 1. Check if post /currency is running
	it('1_test_api_post_currency_returns_status_200_if_complete_input_given', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: 'Europian euro',
	        alias: 'Euro',
	        ex: {
	          'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60
	        }
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(200);
	    });
	});

	// 2. Check if post /currency returns status 400 if name is missing
	it('2_test_api_post_currency_returns_status_400_if_name_is_missing', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        // Missing the 'name' property
	        alias: 'Euro',
	        ex: {
	          'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60
	        }
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	      expect(res.body).to.have.property('error', 'Bad Request - missing required parameter NAME');
	    });
	});
	// 3. Check if post /currency returns status 400 if name is not a string
	it('3_test_api_post_currency_returns_status_400_if_name_is_not_string', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: 14134,
	        alias: 'Euro',
	        ex: {
	          'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60
	        }
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	    });
	});


    // 4. Check if post /currency returns status 400 if name is empty
    it('4_test_api_post_currency_returns_status_400_if_name_is_empty', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: "",
	        alias: 'Euro',
	        ex: {
	          'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60
	        }
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	    });
	});

	//5. Check if post /currency returns status 400 if ex is missing
	 it('5_test_api_post_currency_returns_status_400_if_no_ex', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: "Europian euro",
	        alias: 'Euro',
	       /* ex: {
	          'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60
	        }*/
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	    });
	});

	// 6. Check if post /currency returns status 400 if ex is not an object
	it('6_test_api_post_currency_returns_status_400_if_ex_is_not_an_object', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: "Europian euro",
	        alias: 'Euro',
	        ex: "not an object" // Providing a non-object value for 'ex'
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	    });
	});

	// 7. Check if post /currency returns status 400 if ex is empty
	it('7_test_api_post_currency_returns_400_if_ex_is_empty', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: "Europian euro",
	        alias: 'Euro',
	        ex: {}
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	    });
	});

	//8. Check if post /currency returns status 400 if alias is missing
	it('8_test_api_post_currency_returns_400_if_alias_is_missing', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: "Europian euro",
	        // alias: 'dadd',
	        ex: {
	        'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60}
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	    });
	});

	//9. Check if post /currency returns status 400 if alias is not an string
	it('9_test_api_post_currency_returns_400_if_alias_is_not_an_string', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: "Europian euro",
	        alias: 123145,
	        ex: {
	        'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60}
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	    });
	});

	// 10. Check if post /currency returns status 400 if alias is empty
	it('10_test_api_post_currency_returns_400_if_alias_is_empty', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: "Europian euro",
	        alias: "",
	        ex: {
	        'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60}
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	    });
	});

	//11. Check if post /currency returns status 400 if all fields are complete 
	it('11_test_api_post_currency_returns_400_if_duplicate_alias', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: "Europian euro",
	        alias: "",
	        ex: {
	        'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60}
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(400);
	    });
	});


	// 12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates
	it('1_test_api_post_currency_returns_status_200_if_complete_input_given', () => {
	  chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	      euro: {
	        name: 'Indonesian Rupiah',
	        alias: 'Rupiah',
	        ex: {
	          'peso': 59.75,
	          'usd': 1.06,
	          'won': 1421.85,
	          'yuan': 7.60
	        }
	      }
	    })
	    .end((err, res) => {
	      expect(res.status).to.equal(200);
	    });
	});

	




	
})
